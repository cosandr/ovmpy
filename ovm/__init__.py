"""The OVM module is used to connect to the REST API of the Oracle VM manager.
See the Api class for further details"""

from .api import Api
from .util import wait_for_job, find_by_name
