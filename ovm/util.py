import time
import ovm.api

def wait_for_job(job):
    if job.__class__ != ovm.api.Object or job.id['type'] != "com.oracle.ovm.mgr.ws.model.Job":
        return job
    while not job.done:
        time.sleep(1)
        last = job.latestSummaryProgressMessage
        job = job()
        if last != job.latestSummaryProgressMessage:
            print("%s: %s"%(job.name, job.latestSummaryProgressMessage))
    if job.error is None:
        return job.resultId
    raise Exception(job.error["message"])

def find_by_name(objects, name):
    for obj in objects:
        if obj.name == name:
            return obj
    return None
