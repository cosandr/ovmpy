"""Module for accessing the REST api of the Oracle VM manager"""

import urllib.request
import urllib.parse
import ssl
import base64
import json
import io

def _to_json(obj):
    if obj.__class__ == ObjectType:
        return obj.name
    if obj.__class__ == Object:
        return obj.id
    return obj

def _from_json(api, json_object):
    if "id" in json_object and json_object["id"].__class__ == Object:
        ret = json_object["id"]
        for key, val in json_object.items():
            if key != "id":
                setattr(ret, key, val)
        ret._simple = False
        return ret
    if "type" in json_object and "value" in json_object and "name" in json_object:
        object_type = ObjectType(api, json_object["type"].split(".")[-1])
        return Object(object_type, json_object["value"], json_object["name"])
    return json_object

class Api:
    """Represents a specific connection to an API"""

    def __init__(self, password, username="admin", hostname="localhost", port=7002):
        self.base_uri = 'https://%s:%s/ovm/core/wsapi/rest/'%(hostname, port)

        self.context = ssl.SSLContext()
        self.context.verify_mode = ssl.CERT_NONE
        auth_string = base64.b64encode((username+':'+password).encode()).decode()
        self.auth = "Basic %s" % auth_string

    def _do_request(self, req):
        req.add_header("Authorization", self.auth)
        req.add_header("Accept", "application/json")
        req.add_header("Content-Type", "application/json")

        try:
            resp = urllib.request.urlopen(req, context=self.context)
        except urllib.error.HTTPError as exception:
            print(req.full_url)
            raise exception

        code = resp.status
        if code < 200 or code >= 300:
            raise Exception("Url '%s', code %d: %s"%(req.url, code, resp.reason))
        hook = lambda json_object: _from_json(self, json_object)
        return json.loads(resp.read(), object_hook=hook)

    def _get_uri(self, uri, params=None):
        if not params:
            return self.base_uri+uri
        return self.base_uri+uri+'?'+urllib.parse.urlencode(params)

    def post_json(self, uri, body=None, params=None):
        if body.__class__ != str:
            body = json.dumps(body, default=_to_json)

        req = urllib.request.Request(self._get_uri(uri, params),
                                     data=body.encode('ascii'),
                                     method='POST')
        return self._do_request(req)

    def put_json(self, uri, body=None, params=None):
        buf = io.StringIO()
        json.dump(body, buf, default=_to_json, separators=(',', ':'))
        buf.seek(0)

        req = urllib.request.Request(self._get_uri(uri, params), data=buf, method='PUT')
        return self._do_request(req)

    def delete_json(self, uri, params=None):
        req = urllib.request.Request(self._get_uri(uri, params), method='DELETE')
        return self._do_request(req)

    def get_json(self, uri, params=None):
        req = urllib.request.Request(self._get_uri(uri, params), method='GET')
        return self._do_request(req)

    def __getattr__(self, name):
        object_type = ObjectType(self, name)
        setattr(self, name, object_type)
        return object_type

class ObjectType:
    def __init__(self, api, name):
        if api.__class__ == ObjectType:
            self._api = api._api
        else:
            self._api = api
        self.name = name
        self.uri = self._api.base_uri+name

    def create(self, **kwargs):
        return self._api.post_json(self.name, kwargs)

    def delete(self, obj):
        return self._api.delete_json(self.name + "/" + str(obj))

    def _get_json(self, uri, params=None):
        return self._api.get_json(self.name + "/" + uri, params)

    def _put_json(self, uri, body, params=None):
        return self._api.put_json(self.name + "/" + uri, body, params)

    def __call__(self, **kwargs):
        return self._api.get_json(self.name, kwargs)

    def __getattr__(self, name):
        def action(request_body=None, **kwargs):
            return self._api.post_json(self.name + "/" + name, request_body, kwargs)
        return action

class Object:
    def __init__(self, object_type, id_ref, name, values=None):
        if values is None:
            values = {}
        if object_type.__class__ != ObjectType:
            raise Exception()

        self._object_type = object_type
        self._object_id = id_ref
        self.name = name
        self._simple = not values

        for key, val in values.items():
            setattr(self, key, val)
        self.id = {"type": "com.oracle.ovm.mgr.ws.model."+object_type.name,
                   "value": id_ref,
                   "name": name,
                   "uri": object_type.uri+'/'+id_ref}

    def delete(self):
        return self._object_type.delete(self)

    def __call__(self, **kwargs):
        if not kwargs:
            return self._object_type._get_json(urllib.parse.quote(self._object_id))
        kwargs["id"] = self.id
        return self._object_type._put_json(urllib.parse.quote(self._object_id), kwargs)

    def __getattr__(self, name):
        def action(body=None, **kwargs):
            object_name = urllib.parse.quote(self._object_id) + "/" + name
            if body is None and not kwargs:
                return self._object_type._get_json(object_name)
            return self._object_type._put_json(object_name, body, kwargs)

        if name[0].isupper():
            new_name = self._object_type.name+"/"+self._object_id+"/"+name
            return ObjectType(self._object_type, new_name)
        return action

    def __str__(self):
        return self._object_id

    def __repr__(self):
        return "%s(%s)"%(self._object_type.name, self.name)
