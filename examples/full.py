#!/usr/bin/python3

import ovm
import time

if __name__ == "__main__":
  api = ovm.Api("Welcome1")
  if len(api.Server()) == 0:
    server = ovm.wait_for_job(api.Server.discover("Welcome1", serverName="server01", takeOwnershipIfUnowned=True))
  server = api.Server()[0]
  pool = ovm.find_by_name(api.ServerPool(), "pool1")
  if pool is None:
    pool = ovm.wait_for_job(api.ServerPool.create(name="pool1"))
  ovm.wait_for_job(pool.addServer(server))
  
  if len(server.localFileServer().FileSystem()) == 0:
    ovm.wait_for_job(server.localFileServer().FileSystem.create(name="fileSystem1", storageElementId=server().storageElementIds[0]))
  fileSystem = server.localFileServer().FileSystem()[0]
  
  if len(fileSystem.Repository()) == 0:
    ovm.wait_for_job(fileSystem.Repository.create(name="Repo1"))
  repository = fileSystem.Repository()[0]
  ovm.wait_for_job(repository.present(server))
  
  assembly = ovm.find_by_name(repository.Assembly(), "ol6_latest")
  if assembly is None:
    assembly = ovm.wait_for_job(repository.importAssembly({"urls": ["http://manager/vagrant/output-ovm/ol6_latest.ova"]}))
    assembly(name="ol6_latest")
  
#  network = next(filter(lambda x: x.name == "net", server.Network()), None)
#  if network is None:
#    network = ovm.wait_for_job(server.Network.create(name="net", roles=["VIRTUAL_MACHINE"], ethernetPortIds=[server.EthernetPort()[1]]))
  
  vm = ovm.wait_for_job(assembly.AssemblyVm()[0].createVm({}))
  vm(name="test1", vmDomainType="XEN_HVM_PV_DRIVERS", memory=1024, memoryLimit=1024)
  ovm.wait_for_job(pool.addVm(vm))
  vm.VirtualNic.delete(vm.VirtualNic()[0])
  ovm.wait_for_job(vm.start({}))
  
  time.sleep(10)
  while vm().vmRunState != "STOPPED":
    time.sleep(1)
  
  clone = ovm.wait_for_job(vm.clone(createTemplate=True, serverPoolId=pool))
  clone(name="ol6_latest")
  ovm.wait_for_job(repository.Assembly.delete(assembly))
  disk = vm.VmDiskMapping()[0].virtualDiskId()
  ovm.wait_for_job(vm.delete())
  ovm.wait_for_job(disk.repositoryId.VirtualDisk.delete(disk))
  clone.VmDiskMapping()[0].virtualDiskId(name="system")
  
  print(clone)
