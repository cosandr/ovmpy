#! /usr/bin/python3

import json
import ovm

def fetch_hostvars(vm):
  hostvars = {}
  hostvars["ovm_cpu"] = vm.cpuCount
  hostvars["ovm_memory"] = vm.memory
  hostvars["ovm_description"] = vm.description
  hostvars["ovm_id"] = vm.id["value"]
  hostvars["ovm_state"] = vm.vmRunState
  network = []
  for nic in vm.virtualNicIds:
    nic = nic()
    network.append({
      "macAddress": nic.macAddress,
      "interfaceName": nic.interfaceName,
      "network": nic.networkId.name,
      "ipAddresses": [a["address"] for a in nic.ipAddresses],
    })
  hostvars["ovm_net"] = network
  disks = []
  for disk in vm.vmDiskMappingIds:
    vdisk = disk().virtualDiskId()
    disks.append({
      "name": vdisk.name,
      "description": vdisk.description,
      "diskType": vdisk.diskType,
      "id": vdisk.id["value"],
      "size": vdisk.size,
      "onDiskSize": vdisk.onDiskSize,
      "index": disk().diskTarget,
    })
  hostvars["ovm_disks"] = disks
  return hostvars
   

api = ovm.Api("Welcome1")

groups = {}

groups["ungrouped"] = {"hosts": []}
for group in api.ResourceGroup():
  groups[group.name] = {"hosts": []}

hosts = {}

for vm in api.Vm():
  vm = vm()
  if vm().vmRunState == "TEMPLATE":
    continue
  for group in vm.resourceGroupIds:
    groups[group.name]["hosts"].append(vm.name)
  if len(vm.resourceGroupIds) == 0:
    groups["ungrouped"]["hosts"].append(vm.name)
  hosts[vm.name] = fetch_hostvars(vm)

groups["_meta"] = {"hostvars": hosts}
print(json.dumps(groups))
