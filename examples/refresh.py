#!/usr/bin/python3

import ovm

api = ovm.Api("Welcome1")

for server in api.Server():
  ovm.wait_for_job(server.refresh({}))

for storageArray in api.StorageArray():
  if len(storageArray.adminServer()) > 0:
    ovm.wait_for_job(storageArray.refresh({}))

for fileServer in api.FileServer():
  ovm.wait_for_job(fileServer.refresh({}))

for repository in api.Repository():
  ovm.wait_for_job(repository.refresh({}))

for event in api.Event(severity='CRITICAL'):
  print(event.summary)
